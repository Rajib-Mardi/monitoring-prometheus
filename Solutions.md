
<summary>Exercise 2: Start Monitoring your Applications </summary>
 <br />

**steps**
```sh
# Create docker-registry secret
DOCKER_REGISTRY_SERVER=docker.io
DOCKER_USER=your dockerID, same as for `docker login`
DOCKER_EMAIL=your dockerhub email, same as for `docker login`
DOCKER_PASSWORD=your dockerhub pwd, same as for `docker login`

kubectl create secret -n my-app docker-registry my-registry-key \
--docker-server=$DOCKER_REGISTRY_SERVER \
--docker-username=$DOCKER_USER \
--docker-password=$DOCKER_PASSWORD \
--docker-email=$DOCKER_EMAIL


# Deploy promentheus operator
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

kubectl create namespace monitoring
helm install monitoring-stack prometheus-community/kube-prometheus-stack -n monitoring

# Access Prometheus UI and view its targets
kubectl port-forward svc/monitoring-stack-kube-prom-prometheus 9090:9090
http://127.0.0.1:9090/targets

# Clean up and prepare for new installation
helm uninstall mysql-release
helm uninstall ingress-controller -n ingress

# NOTE:
# We are using "release: monitoring-stack" label to expose scrape endpoint. This label may change with newer prometheus stack version, so to check which label you need to apply, do the following
# - Get name of the prometheus CRD
kubectl get prometheuses.monitoring.coreos.com
# - Print out the ServiceMonitor selector
kubectl get prometheuses.monitoring.coreos.com {crd-name} -o yaml | grep serviceMonitorSelector -A 2

# Build and use the correct java-app image with metrics exposed:
# check out the java app code with prometheus client inside
git checkout feature/monitoring
# build a new jar
./gradlew clean build
# build a docker image
docker build {docker-hub-id}:{repo-name}:{tag} .
docker push {docker-hub-id}:{repo-name}:{tag}
# set the correct image name "{docker-hub-id}:{repo-name}:{tag}" in "kubernetes-manifests/java-app.yaml" file


# To add metrics scraping to nginx, mysql and java apps, execute ansible playbook
ansible-playbook 2-configure-k8s.yaml

# Access Prometheus UI and see that new targets for mysql, nginx and your java application have been added
http://127.0.0.1:9090/targets
```

</details>
